This quantitative data captures the current state of digital skills at the Organisational and Government level. The data also captures the current influence of technological advancements commonly known as the 4th industrial revolution (4IR) on digital skills. 

The study sought to understand how digital technologies are understood across the different sectors of South Africa, and the status of digital skills. 

Five areas were investigated: 
�	How digital technologies are understood
�	Digital skills deployment (usage)
�	Digital skills development (training)
�	Digital skills supply (recruitment and retainment)
�	Digital skills demand (needs).

The quantitative study involved 597 respondents from the 21 sectors defined by the South African standard industrial classification (Statistics South Africa). data consusts of responses from 110 HR Managers, 360 Employees and 127 Managers from various organisations in each of the sectors.

