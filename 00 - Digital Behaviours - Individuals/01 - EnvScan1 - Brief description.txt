The data on the "Environmental scan on digital skills in South Africa at the citizen level" was collected from across all provinces and populations in South Africa seeking to understanding the state of digital behaviours associated with individuals. The study was exploratory in nature as most existing surveys either focused on practitioner IT skills or access/usage of digital technology. The data sought to go beyond these to include nine aspects:

�	Digital ownership;
�	Digital access;
�	Digital awareness;
�	Digital usage;
�	Digital benefits (including digital social inclusion and digital economic inclusion) ; 
�	21st century skills;
�	ICT self-efficacy; 
�	Government to citizen interaction using digital platforms; 
�	Poverty and social inclusion.

The relationships between these aspects are critical to understanding digital skills interventions at a national, provincial and local level.

The data should be able to help understand how digital technologies, now integrated into daily living and are increasingly integral to economic activity under the 4IR, and how the behaviours might be productively and meaningfully used by individuals, organisations and communities. The results from the data are designed to inform evidence-based decision-making on digital skills in South Africa.

Data was collected between January and April 2019.

For more information, please visit www.nemisa.co.za.